<?php
session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['role'])) {
    header("Location: login.html");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

$username = $_SESSION['username'];
$role = $_SESSION['role'];

$user_id = null;

if ($role == 'student') {
    $stmt = $conn->prepare("SELECT id FROM studenti WHERE username = ?");
    $stmt->bind_param("s", $username);
} else if ($role == 'tutore') {
    $stmt = $conn->prepare("SELECT id FROM tutore WHERE username = ?");
    $stmt->bind_param("s", $username);
}

$stmt->execute();
$result = $stmt->get_result();
if ($row = $result->fetch_assoc()) {
    $user_id = $row['id'];
}

$receiver_id = $_GET['receiver_id'];

$stmt = $conn->prepare("SELECT messages.sender_id, messages.receiver_id, messages.message, 
                               IFNULL(studenti.username, tutore.username) AS sender_username
                        FROM messages
                        LEFT JOIN studenti ON messages.sender_id = studenti.id
                        LEFT JOIN tutore ON messages.sender_id = tutore.id
                        WHERE (messages.sender_id = ? AND messages.receiver_id = ?) 
                           OR (messages.sender_id = ? AND messages.receiver_id = ?)
                        ORDER BY messages.timestamp ASC");
$stmt->bind_param("iiii", $user_id, $receiver_id, $receiver_id, $user_id);
$stmt->execute();
$result = $stmt->get_result();

$messages = [];
while ($row = $result->fetch_assoc()) {
    $messages[] = $row;
}

$stmt->close();
$conn->close();

echo json_encode($messages);
?>
