<?php
session_start();

if (!isset($_SESSION['username']) || $_SESSION['role'] != 'tutore') {
    header("Location: login.html");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $lectie_id = $_POST['lectie_id'];
    $titlu_tema = $_POST['titlu_tema'];
    $continut_tema = $_POST['continut_tema'];
    
    $stmt = $conn->prepare("INSERT INTO teme (lectie_id, titlu, continut) VALUES (?, ?, ?)");
    $stmt->bind_param("iss", $lectie_id, $titlu_tema, $continut_tema);
    
    if ($stmt->execute()) {
        echo "Tema a fost adăugată cu succes.";
    } else {
        echo "Eroare la adăugarea temei: " . $stmt->error;
    }
    
    $stmt->close();
}

$conn->close();

header("Location: tutore.php");
exit();
?>
