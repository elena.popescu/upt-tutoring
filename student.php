<?php
session_start();

if (!isset($_SESSION['username']) || $_SESSION['role'] != 'student') {
    header("Location: login.html");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");
    
if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}
$stmt = $conn->prepare("SELECT id FROM studenti WHERE username = ?");
$stmt->bind_param("s", $_SESSION['username']);
$stmt->execute();
$result = $stmt->get_result();

$student_id = null;
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $student_id = $row['id'];
}
$stmt->close();

if ($student_id) {
    $stmt = $conn->prepare("SELECT cursuri.id, cursuri.titlu FROM cursuri 
                            JOIN studenti_cursuri ON cursuri.id = studenti_cursuri.curs_id 
                            WHERE studenti_cursuri.student_id = ?");
    $stmt->bind_param("i", $student_id);
    $stmt->execute();
    $cursuri_result = $stmt->get_result();
}
?>
<!DOCTYPE html>
<style>
    header{
        background-color: rgb(11, 0, 106);
        text-align: center;
        width: 195vh;
        height: 100px;
        font-size: 80px;
        color: aliceblue;
        border-radius: 10px;
    
    }
    .message{
        background-color: rgb(212, 235, 255);
    margin-top:15px;
    margin-left: 150px;
    margin-right: 150px;
    text-align: center;
    height: 550px;
    font-size: 60px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 10px;
    }
    .start{
        font-family: 'Times New Roman', Times, serif;
        font-style:italic;
        font-size: 30 px;
        color:rgb(11, 0, 106);
        border-bottom-style: solid;
    }
   
   button{
    border-radius: 10px;
    font-size: 50px;
    margin-left: 40px;
    margin-right: 40px;
    background-color:  rgb(212, 235, 255);
    color: rgb(11, 0, 106);
    border-width: 0px;
    width: 300px;
    height: 300px;
    align-items: center;
    margin-bottom: 40px;
   }
   .sidebar {
    
    width: 130px;
    height: 100vh;
    position: fixed;
    background: white;
    padding-top: 10px;
    font-size: 20px;
    
}
main{
    margin-left: 120px;
}

.chat{
    border-radius: 0px;
    background-color:white;
    color:rgb(11, 0, 106);
    margin-left: 450px;
    border-width: 0px;
    font-family: 'Times New Roman', Times, serif;
    margin-right: 0px;
    


   }
   .logout{
  margin-left: 450px;

   }
        

   

    
    </style>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>UPT TUTORING</title>
  <header>
    Welcome UPT Tutoring!
</header>
</head>
<body>
    <h2 class="start">Bun venit, <?php echo htmlspecialchars($_SESSION['username']); ?>!
    <a href="chat.php" class="chat">Chat</a>
    <a href="logout.php" class="logout">Logout</a>
</h2>
<br><br>

<?php
$titluri[100]=0;
$det_cursuri[100]=0;
$nr_cursuri=0;
echo "<div class='sidebar'>
<p>Cursurile tale<p>";
if ($cursuri_result->num_rows > 0) {
while ($curs = $cursuri_result->fetch_assoc()) {
$curs_id = $curs['id'];
$titlu = htmlspecialchars($curs['titlu']);
echo "
<a href='detalii_curs.php?curs_id={$curs_id}' style='color:black;font-style:italic;'>{$titlu}</a>
<br>
<br>";
$titluri[++$nr_cursuri]=$titlu;
$det_cursuri[$nr_cursuri]=$curs_id;
}
echo"</div>";
} else {
echo "<p>No courses assigned.</p>";
}
echo"<main>";
while($nr_cursuri>0){
echo "<a href='detalii_curs.php?curs_id={$det_cursuri[$nr_cursuri]}'><button>{$titluri[$nr_cursuri]}</button></a>";
$nr_cursuri=$nr_cursuri-1;
}
echo"</main>";
?>

<br>
<br>
</body>
</html>