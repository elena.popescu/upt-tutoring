<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>UPT TUTORING</title>
    <style>
        header {
            background-color: rgb(11, 0, 106);
            text-align: center;
            width: 195vh;
            height: 100px;
            font-size: 80px;
            color: aliceblue;
            border-radius: 10px;
        }
        ul {
            list-style-type: none;
            padding: 0;
        }
        li {
            background-color: #f2f2f2;
            margin: 10px 0;
            padding: 20px;
            border-radius: 10px;
            font-size: 18px;
        }
        .response, .feedback {
            white-space: pre-wrap;
            font-size: 16px;
            margin-top: 10px;
        }
        form {
            margin-top: 10px;
        }
        textarea {
            width: 100%;
            height: 100px;
        }
    </style>
</head>
<body>
    <header>
        Welcome UPT Tutoring!
    </header>
    <h2>Temele încărcate</h2>

    <form method="GET" action="view_tema.php">
        <label for="search">Caută tema sau studentul după nume:</label>
        <input type="text" id="search" name="search" value="<?php echo isset($_GET['search']) ? htmlspecialchars($_GET['search']) : ''; ?>">
        <input type="submit" value="Caută">
    </form>

    <?php
    session_start();

    if (!isset($_SESSION['username']) || !isset($_SESSION['role'])) {
        header("Location: login.html");
        exit();
    }

    if ($_SESSION['role'] != 'tutore') {
        echo "Doar tutorii pot vizualiza temele.";
        exit();
    }

    $conn = new mysqli("localhost", "root", "", "Licenta");
    if ($conn->connect_error) {
        die("Conexiune eșuată: " . $conn->connect_error);
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['tema_id']) && isset($_POST['feedback'])) {
        $tema_id = $_POST['tema_id'];
        $feedback = $_POST['feedback'];

        $stmt = $conn->prepare("UPDATE teme SET feedback = ? WHERE id = ?");
        if ($stmt === false) {
            die("Eroare la pregătirea interogării: " . $conn->error);
        }
        $stmt->bind_param("si", $feedback, $tema_id);
        if ($stmt->execute()) {
            echo "Feedback-ul a fost trimis cu succes.";
        } else {
            echo "Eroare la trimiterea feedback-ului: " . $stmt->error;
        }

        $stmt->close();
    }

    $username = $_SESSION['username'];
    $stmt = $conn->prepare("SELECT id FROM tutore WHERE username = ?");
    if ($stmt === false) {
        die("Eroare la pregătirea interogării: " . $conn->error);
    }
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->bind_result($tutore_id);
    $stmt->fetch();
    $stmt->close();

    if (!$tutore_id) {
        echo "Eroare: Nu s-a putut obține ID-ul tutorelui.";
        exit();
    }

    $stmt = $conn->prepare("SELECT curs_id FROM tutori_cursuri WHERE tutore_id = ?");
    if ($stmt === false) {
        die("Eroare la pregătirea interogării: " . $conn->error);
    }
    $stmt->bind_param("i", $tutore_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $curs_ids = [];
    while ($row = $result->fetch_assoc()) {
        $curs_ids[] = $row['curs_id'];
    }
    $stmt->close();

    if (empty($curs_ids)) {
        echo "Nu există cursuri asociate cu acest tutore.";
        exit();
    }

    $curs_ids_str = implode(',', $curs_ids);

    $sql = "SELECT lectie_id FROM cursuri_lectii WHERE curs_id IN ($curs_ids_str)";
    $result = $conn->query($sql);
    if ($result === false) {
        die("Eroare la executarea interogării: " . $conn->error);
    }
    $lectie_ids = [];
    while ($row = $result->fetch_assoc()) {
        $lectie_ids[] = $row['lectie_id'];
    }

    if (empty($lectie_ids)) {
        echo "Nu există lecții asociate cu cursurile predate de acest tutore.";
        exit();
    }

    $lectie_ids_str = implode(',', $lectie_ids);

    $search = isset($_GET['search']) ? '%' . $conn->real_escape_string($_GET['search']) . '%' : '%';
    $sql = "SELECT t.id, t.nume_tema, t.raspuns, t.data_adaugare, s.username, t.feedback
            FROM teme t 
            JOIN studenti s ON t.student_id = s.id 
            WHERE t.lectie_id IN ($lectie_ids_str) 
            AND (t.nume_tema LIKE ? OR s.username LIKE ?)";

    $stmt = $conn->prepare($sql);
    if ($stmt === false) {
        die("Eroare la pregătirea interogării: " . $conn->error);
    }
    $stmt->bind_param("ss", $search, $search);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        echo "<ul>";
        while ($row = $result->fetch_assoc()) {
            echo "<li><strong>Titlu:</strong> " . htmlspecialchars($row['nume_tema']) . 
                 "<div class='response'><strong>Răspuns:</strong> " . htmlspecialchars($row['raspuns']) . 
                 "</div><strong>Student:</strong> " . htmlspecialchars($row['username']) . 
                 " | <strong>Data adăugării:</strong> " . htmlspecialchars($row['data_adaugare']) . 
                 "<div class='feedback'><strong>Feedback:</strong> " . htmlspecialchars($row['feedback']) . 
                 "</div><form method='POST' action=''>
                    <input type='hidden' name='tema_id' value='" . htmlspecialchars($row['id']) . "'>
                    <textarea name='feedback' placeholder='Adaugă feedback aici...'></textarea>
                    <input type='submit' value='Trimite Feedback'>
                 </form></li>";
        }
        echo "</ul>";
    } else {
        echo "Nu există teme încărcate pentru lecțiile predate de acest tutore.";
    }

    $stmt->close();
    $conn->close();
    ?>
</body>
</html>
