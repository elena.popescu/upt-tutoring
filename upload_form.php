<?php
session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['role'])) {
    $_SESSION['notification'] = ['message' => 'Trebuie să vă autentificați.', 'type' => 'error'];
    header("Location: login.html");
    exit();
}

if ($_SESSION['role'] != 'student') {
    $_SESSION['notification'] = ['message' => 'Doar studenții pot vizualiza temele.', 'type' => 'error'];
    header("Location: index.php");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");
if ($conn->connect_error) {
    $_SESSION['notification'] = ['message' => 'Conexiune eșuată: ' . $conn->connect_error, 'type' => 'error'];
    header("Location: index.php");
    exit();
}

$username = $_SESSION['username'];
$stmt = $conn->prepare("SELECT id FROM studenti WHERE username = ?");
$stmt->bind_param("s", $username);
$stmt->execute();
$stmt->bind_result($student_id);
$stmt->fetch();
$stmt->close();

if (!$student_id) {
    $_SESSION['notification'] = ['message' => 'Eroare: Nu s-a putut obține ID-ul studentului.', 'type' => 'error'];
    header("Location: index.php");
    exit();
}

if (isset($_POST["raspuns"]) && isset($_POST["nume_tema"])) {
    $lectie_id = 1; 
    
    $stmt = $conn->prepare("INSERT INTO teme (lectie_id, raspuns, nume_tema, student_id) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("issi", $lectie_id, $_POST['raspuns'], $_POST['nume_tema'], $student_id);
    $stmt->execute();

    if ($stmt->affected_rows > 0) {
        $_SESSION['notification'] = ['message' => 'Tema a fost încărcată cu succes.', 'type' => 'success'];
    } else {
        $_SESSION['notification'] = ['message' => 'Eroare: Nu s-a putut insera tema în baza de date.', 'type' => 'error'];
    }

    $stmt->close();
    header("Location: {$_SERVER['PHP_SELF']}");
    exit();
}

$stmt = $conn->prepare("SELECT t.nume_tema, t.raspuns, t.feedback, l.titlu AS lectie_titlu
                        FROM teme t
                        JOIN lectii l ON t.lectie_id = l.id
                        WHERE t.student_id = ?");
$stmt->bind_param("i", $student_id);
$stmt->execute();
$result = $stmt->get_result();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>UPT TUTORING</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        header {
            background-color: rgb(11, 0, 106);
            text-align: center;
            width: 195vh;
            height: 100px;
            font-size: 80px;
            color: aliceblue;
            border-radius: 10px;
        }
        .section {
            margin-top: 20px;
            margin-left: 150px;
            margin-right: 150px;
            font-size: 20px;
        }
        .teme {
            background-color: rgb(212, 235, 255);
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .teme h3 {
            font-style: italic;
            font-size: 24px;
            margin-bottom: 10px;
        }
        .teme ul {
            list-style-type: none;
            padding: 0;
            text-align: left;
        }
        .teme li {
            background-color: whitesmoke;
            border-radius: 10px;
            margin-bottom: 20px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 10px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: left;

        }
        .teme li:last-child {
            border-bottom: none;
        }
        .raspuns {
            width: 100%;
            min-height: 100px;
            max-height: 200px;
            resize: vertical;
            padding: 5px;
            font-size: 16px;
        }
        .submit:hover {
            background-color: navy;
            color: white;
        }
        .submit {
            font-style: italic;
            border: 0;
            font-size: 16px;
            background-color: white;
            color: navy;
            cursor: pointer;
            padding: 10px 20px;
            margin-top: 10px;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <header>
        Welcome UPT Tutoring!
    </header>
    <div>
        <h3>Încărcare și Vizualizare Teme</h3>
        <div class="teme">
            <h4>Încărcare Temă</h4>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <label for="nume_tema">Titlu:</label><br>
                <input type="text" id="nume_tema" name="nume_tema" required style="width: 400px;"><br><br>

                <label for="raspuns">Răspuns:</label><br>
                <textarea id="raspuns" name="raspuns" required class="raspuns"></textarea><br><br>

                <input type="submit" value="Încarcă Temă" class="submit">
            </form>
        </div>
        <hr>
        <div class="teme">
            <h4>Temele Încărcate</h4>
            <?php if ($result->num_rows > 0): ?>
                <ul>
                    <?php while ($row = $result->fetch_assoc()): ?>
                        <li>
                            <strong style="margin-left: 10px; margin-top:10px; margin-bottom:10px;">Nume Temă:</strong> <?php echo htmlspecialchars($row['nume_tema']); ?><br>
                            <strong style="margin-left: 10px; margin-top:10px; margin-bottom:10px;">Răspuns:</strong><br>
                            <div style="margin-left: 10px; margin-top:10px; margin-bottom:10px;white-space: pre-wrap;"><?php echo htmlspecialchars($row['raspuns']); ?></div><br>
                            <?php if (!empty($row['feedback'])): ?>
                                <strong style="margin-left: 10px; margin-top:10px; margin-bottom:10px;">Feedback:</strong><br>
                                <div style="margin-left: 10px; margin-top:10px; margin-bottom:10px;white-space: pre-wrap;"><?php echo htmlspecialchars($row['feedback']); ?></div><br>
                            <?php endif; ?>
                            <strong style="margin-left: 10px; margin-top:10px; margin-bottom:10px;">Lecție:</strong> <?php echo htmlspecialchars($row['lectie_titlu']); ?><br>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php else: ?>
                <p>Nu ai încărcat încă nicio temă.</p>
            <?php endif; ?>
        </div>
    </div>
    <br>
    <a href="student.php" style="font-size: 20px; color: blue;">Înapoi la Pagina Principală</a>
</body>
</html>

<?php
$stmt->close();
$conn->close();
?>
