-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1:3306
-- Timp de generare: iun. 19, 2024 la 01:19 PM
-- Versiune server: 5.7.40
-- Versiune PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `licenta`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `cursuri`
--

DROP TABLE IF EXISTS `cursuri`;
CREATE TABLE IF NOT EXISTS `cursuri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titlu` varchar(255) NOT NULL,
  `descriere` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `cursuri`
--

INSERT INTO `cursuri` (`id`, `titlu`, `descriere`) VALUES
(1, 'Curs C++', 'Curs introductiv de programare în limbajul C++'),
(2, 'Curs Java', 'Curs introductiv de programare în limbajul Java'),
(3, 'Curs C', 'Curs introductiv de programare în limbajul Python'),
(4, 'Curs HTLM', 'Curs introductiv de creare a unei pagini web folosind HTML');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `cursuri_lectii`
--

DROP TABLE IF EXISTS `cursuri_lectii`;
CREATE TABLE IF NOT EXISTS `cursuri_lectii` (
  `curs_id` int(11) NOT NULL,
  `lectie_id` int(11) NOT NULL,
  PRIMARY KEY (`curs_id`,`lectie_id`),
  KEY `lectie_id` (`lectie_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `cursuri_lectii`
--

INSERT INTO `cursuri_lectii` (`curs_id`, `lectie_id`) VALUES
(1, 1),
(1, 2),
(1, 15),
(1, 16),
(2, 3),
(2, 11),
(2, 12),
(2, 13),
(3, 4),
(3, 14),
(4, 5),
(4, 6);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `lectii`
--

DROP TABLE IF EXISTS `lectii`;
CREATE TABLE IF NOT EXISTS `lectii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titlu` varchar(255) NOT NULL,
  `continut` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `lectii`
--

INSERT INTO `lectii` (`id`, `titlu`, `continut`) VALUES
(1, 'C++ Variables and Types', 'Data types\nC++ provides a rich set of built-in as well as user defined data types. Following table lists down seven basic C++ data types:\nBoolean - boolean - either true or false\nCharacters - alphabets and all the symbols. Defined using char.\nIntegers - whole numbers which can be both positive and negative. Defined using int (4 bytes) or short int (2 bytes) or long int (8 bytes) based on the size of the numbers used.\nFloating point numbers - real numbers (numbers with fractions). Defined using float and double.\nValueless using the void keyword\nWide character using the wchar_t keyword\nDefining variables\nFor numbers, we will usually use the type int, which an integer in the size of a \"word\" the default number size of the machine which your program is compiled on. On most computers today, it is a 32-bit number, which means the number can range from -2,147,483,648 to 2,147,483,647 (same as long).'),
(2, 'C++ Hello World!', 'Introduction\nC++ (pronounced see plus plus) is a general purpose programming language that is free-form and compiled. It is regarded as an intermediate-level language, as it comprises both high-level and low-level language features. It provides imperative, object-oriented and generic programming features.\n\nC++ is one of the most popular programming languages and is implemented on a wide variety of hardware and operating system platforms. As an efficient performance driven programming language it is used in systems software, application software, device drivers, embedded software, high-performance server and client applications, and entertainment software such as video games. Various entities provide both open source and proprietary C++ compiler software, including the FSF, LLVM, Microsoft and Intel.\n'),
(3, 'Java Variables and Types', 'Variables and Types\nAlthough Java is object oriented, not all types are objects. It is built on top of basic variable types called primitives.\n\nHere is a list of all primitives in Java:\n\nbyte (number, 1 byte)\nshort (number, 2 bytes)\nint (number, 4 bytes)\nlong (number, 8 bytes)\nfloat (float number, 4 bytes)\ndouble (float number, 8 bytes)\nchar (a character, 2 bytes)\nboolean (true or false, 1 byte)\nJava is a strong typed language, which means variables need to be defined before we use them.'),
(4, 'C Functions', 'Functions\nC functions are simple, but because of how C works, the power of functions is a bit limited.\n\nFunctions receive either a fixed or variable amount of arguments.\nFunctions can only return one value, or return no value.\nIn C, arguments are copied by value to functions, which means that we cannot change the arguments to affect their value outside of the function. To do that, we must use pointers, which are taught later on.'),
(5, 'HTML Basic Elements', 'Basic Elements\nThe basic elements of an HTML page are:\n\nA text header, denoted using the <h1>, <h2>, <h3>, <h4>, <h5>, <h6> tags.\nA paragraph, denoted using the <p> tag.\nA horizontal ruler, denoted using the <hr> tag.\nA link, denoted using the <a> (anchor) tag.\nA list, denoted using the <ul> (unordered list), <ol> (ordered list) and <li> (list element) tags.\nAn image, denoted using the <img> tag\nA divider, denoted using the <div> tag\nA text span, denoted using the <span> tag\nThe next few pages will give an overview of these basic HTML elements.\n\nEach element can also have attributes - each element has a different set of attributes relevant to the element. There are a few global elements, the most common of them are:\n\nid - Denotes the unique ID of an element in a page. Used for locating elements by using links, JavaScript, and more.\nclass - Denotes the CSS class of an element. Explained in the CSS Basics tutorial.\nstyle - Denotes the CSS styles to apply to an element. Explained in the CSS Basics tutorial.\ndata-x attributes - A general prefix for attributes that store raw information for programmatic purposes. Explained in detailed in the Data Attributes section.'),
(6, 'HTML Introduction', 'What is HTML?\nHTML stands for Hyper Text Markup Language\nHTML is the standard markup language for creating Web pages\nHTML describes the structure of a Web page\nHTML consists of a series of elements\nHTML elements tell the browser how to display the content\nHTML elements label pieces of content such as \"this is a heading\", \"this is a paragraph\", \"this is a link\", etc.\nA Simple HTML Document\nExample\n<!DOCTYPE html>\n<html>\n<head>\n<title>Page Title</title>\n</head>\n<body>\n\n<h1>My First Heading</h1>\n<p>My first paragraph.</p>\n\n</body>\n</html>\nExample Explained\nThe <!DOCTYPE html> declaration defines that this document is an HTML5 document\nThe <html> element is the root element of an HTML page\nThe <head> element contains meta information about the HTML page\nThe <title> element specifies a title for the HTML page (which is shown in the browser is title bar or in the page is tab)\nThe <body> element defines the document is body, and is a container for all the visible contents, such as headings, paragraphs, images, hyperlinks, tables, lists, etc.\nThe <h1> element defines a large heading\nThe <p> element defines a paragraph\nWhat is an HTML Element?\nAn HTML element is defined by a start tag, some content, and an end tag:\n\n<tagname> Content goes here... </tagname>\nThe HTML element is everything from the start tag to the end tag:\n\n<h1>My First Heading</h1>\n<p>My first paragraph.</p>'),
(13, 'Arrays', 'Arrays in Java are also objects. They need to be declared and then created. In order to declare a variable that will hold an array of integers, we use the following syntax:\r\n\r\nint[] arr;\r\nNotice there is no size, since we didn\'t create the array yet.\r\n\r\narr = new int[10];\r\nThis will create a new array with the size of 10. We can check the size by printing the array\'s length:\r\n\r\nSystem.out.println(arr.length);\r\nWe can access the array and set values:\r\n\r\narr[0] = 4;\r\narr[1] = arr[0] + 5;\r\nJava arrays are 0 based, which means the first element in an array is accessed at index 0 (e.g: arr[0], which accesses the first element). Also, as an example, an array of size 5 will only go up to index 4 due to it being 0 based.\r\n\r\nint[] arr = new int[5];\r\n//accesses and sets the first element\r\narr[0] = 4;\r\nWe can also create an array with values in the same line:\r\n\r\nint[] arr = {1, 2, 3, 4, 5};\r\nDon\'t try to print the array without a loop, it will print something nasty like [I@f7e6a96. To print out an array, use the following code:\r\n\r\nfor (int i=0; i < arr.length; i++) {\r\n    System.out.println(arr[i]);\r\n}'),
(14, 'Hello, world', 'Introduction\r\nThe C programming language is a general purpose programming language, which relates closely to the way machines work. Understanding how computer memory works is an important aspect of the C programming language. Although C can be considered as \"hard to learn\", C is in fact a very simple language, with very powerful capabilities.\r\n\r\nC is a very common language, and it is the language of many applications such as Windows, the Python interpreter, Git, and many many more.\r\n\r\nC is a compiled language - which means that in order to run it, the compiler (for example, GCC or Visual Studio) must take the code that we wrote, process it, and then create an executable file. This file can then be executed, and will do what we intended for the program to do.\r\n\r\nOur first program\r\nEvery C program uses libraries, which give the ability to execute necessary functions. For example, the most basic function called printf, which prints to the screen, is defined in the stdio.h header file.\r\n\r\nTo add the ability to run the printf command to our program, we must add the following include directive to our first line of the code:\r\n\r\n#include <stdio.h>\r\nThe second part of the code is the actual code which we are going to write. The first code which will run will always reside in the main function.\r\n\r\nint main() {\r\n  ... our code goes here\r\n}\r\nThe int keyword indicates that the function main will return an integer - a simple number. The number which will be returned by the function indicates whether the program that we wrote worked correctly. If we want to say that our code was run successfully, we will return the number 0. A number greater than 0 will mean that the program that we wrote failed.\r\n\r\nFor this tutorial, we will return 0 to indicate that our program was successful:\r\n\r\nreturn 0;\r\nNotice that every statement in C must end with a semicolon, so that the compiler knows that a new statement has started.\r\n\r\nLast but not least, we will need to call the function printf to print our sentence.');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `receiver_id` (`receiver_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `receiver_id`, `message`, `timestamp`) VALUES
(3, 765438, 357159, 'Buna ziua!', '2024-05-28 14:04:46'),
(4, 357159, 765438, 'Buna ziua!', '2024-05-28 14:05:42'),
(5, 357159, 765438, 'buna', '2024-05-28 14:20:26'),
(6, 357159, 765438, 'bunaa', '2024-05-28 14:21:29'),
(7, 357159, 765438, 'bunaaa', '2024-05-28 16:05:19'),
(8, 765438, 357159, 'Buna', '2024-05-29 10:32:18'),
(9, 357159, 765438, 'aaa', '2024-05-29 14:02:05'),
(10, 765438, 357159, 'bbb', '2024-05-30 17:07:32'),
(11, 765438, 357159, 'ccc', '2024-05-31 12:56:50'),
(12, 765438, 357159, 'pa pa', '2024-06-03 12:51:39'),
(13, 765438, 654321, 'buna ziua!', '2024-06-05 08:55:46'),
(14, 654321, 765438, 'buna', '2024-06-05 08:57:17'),
(15, 654321, 129854, 'Buna ziua!', '2024-06-05 09:05:13'),
(16, 129854, 654321, 'buna ziua', '2024-06-05 09:06:17'),
(17, 654321, 129854, 'buna', '2024-06-05 12:03:23'),
(18, 613166, 357159, 'Buna ziua!', '2024-06-17 11:32:19');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `profesori`
--

DROP TABLE IF EXISTS `profesori`;
CREATE TABLE IF NOT EXISTS `profesori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `profesori`
--

INSERT INTO `profesori` (`id`, `username`, `password`) VALUES
(1, 'PopescuMariana1', 'admin'),
(2, 'IoanaGrigore2', 'admin'),
(3, 'IancuVioleta3', 'admin');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `studenti`
--

DROP TABLE IF EXISTS `studenti`;
CREATE TABLE IF NOT EXISTS `studenti` (
  `ID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `studenti`
--

INSERT INTO `studenti` (`ID`, `Username`, `Password`) VALUES
(765438, 'IonescuMaria12', 'bywg64rvde'),
(129854, 'PopescuAna78', 'kjhsf32hsd'),
(487256, 'VasileAlex99', 'lmn4ty3pqo'),
(963258, 'GeorgescuIoan45', 'zxcv76phm'),
(357951, 'RadulescuElena87', 'qwer98asd'),
(631245, 'DumitrescuAndrei23', 'yxcv35bnm'),
(874563, 'StanescuAlina56', 'mnbv64fdsa'),
(248975, 'PopaIonut67', 'poiuy76tgb'),
(582136, 'GheorgheDenisa88', 'zxcv25mlp'),
(654879, 'TudorStefan99', 'asdf87hjk'),
(123456, 'AndreiPopescu22', 'qwer12yui'),
(987654, 'ElenaIonescu11', 'pktnb98vcx'),
(456123, 'MariusGeorgescu33', '123qweasd'),
(789456, 'AnaMariaIvan99', 'asdf456jkl'),
(613166, 'elena.popescu.0921', 'admin');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `studenti_cursuri`
--

DROP TABLE IF EXISTS `studenti_cursuri`;
CREATE TABLE IF NOT EXISTS `studenti_cursuri` (
  `student_id` int(11) NOT NULL,
  `curs_id` int(11) NOT NULL,
  PRIMARY KEY (`student_id`,`curs_id`),
  KEY `curs_id` (`curs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `studenti_cursuri`
--

INSERT INTO `studenti_cursuri` (`student_id`, `curs_id`) VALUES
(129854, 1),
(129854, 4),
(487256, 2),
(613166, 1),
(613166, 2),
(613166, 3),
(613166, 4),
(765438, 1),
(765438, 3),
(765438, 4);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `teme`
--

DROP TABLE IF EXISTS `teme`;
CREATE TABLE IF NOT EXISTS `teme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lectie_id` int(11) DEFAULT NULL,
  `continut` text,
  `titlu` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `data_adaugare` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `raspuns` text,
  `nume_tema` text,
  `feedback` text,
  `tutore_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lectie_id` (`lectie_id`),
  KEY `fk_tutore` (`tutore_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `teme`
--

INSERT INTO `teme` (`id`, `lectie_id`, `continut`, `titlu`, `student_id`, `data_adaugare`, `raspuns`, `nume_tema`, `feedback`, `tutore_id`) VALUES
(1, 1, 'Sa se scrie un program care citeste de la tastatura doua numere naturale si determina suma lor.', 'suma', NULL, '2024-05-29 15:06:29', '', '', NULL, NULL),
(5, 1, NULL, NULL, 765438, '2024-05-29 18:01:39', '#include <iostream>\r\n\r\nusing namespace std;\r\n\r\nint main()\r\n{int n,m,s=0;\r\ncin>>n>>m;\r\ns=n+m;\r\n    cout <<s;\r\n}\r\n', 'suma', 'foarte bine', NULL),
(6, 2, 'Se dau 2 numere naturale. Calculati diferena lor.\r\n\r\n', 'diferenta', NULL, '2024-05-30 10:23:12', NULL, NULL, NULL, NULL),
(7, 1, NULL, NULL, 765438, '2024-05-30 10:24:28', '#include <iostream>\r\n\r\nusing namespace std;\r\n\r\nint main()\r\n{int n,m,d=0,t=0;\r\ncin>>n>>m;\r\nif(n<m){t=n;n=m;m=t;}\r\nd=n-m;\r\n    cout <<d;\r\n}', 'diferenta', NULL, NULL),
(8, 1, NULL, NULL, 129854, '2024-05-30 16:10:12', '#include <iostream>\r\nusing namespace std;\r\nint a , b , d;\r\nint main() \r\n{\r\n    cin >> a >> b;\r\n    d = a - b;\r\n    cout << d;\r\n    return 0; \r\n}', 'diferenta', NULL, NULL),
(9, 1, NULL, NULL, 129854, '2024-05-30 16:22:26', '#include <iostream>\r\nusing namespace std;\r\nint a , b , d;\r\nint main() \r\n{\r\n    cin >> a >> b;\r\n    d = a - b;\r\n    cout << d;\r\n    return 0; \r\n}', 'diferenta', NULL, NULL),
(10, 1, NULL, NULL, 129854, '2024-05-30 16:23:22', '#include <iostream>\r\nusing namespace std;\r\n\r\nint suma(int a ,int b)\r\n{\r\n    int s = a + b;\r\n    return s;\r\n}\r\n\r\nint main()\r\n{\r\n    int a , b;\r\n    cin >> a >> b;\r\n    cout << suma(a , b);\r\n    \r\n    return 0;\r\n}', 'suma', NULL, NULL),
(11, 1, NULL, NULL, 129854, '2024-05-30 16:27:20', '#include <iostream>\r\nusing namespace std;\r\n\r\nint suma(int a ,int b)\r\n{\r\n    int s = a + b;\r\n    return s;\r\n}\r\n\r\nint main()\r\n{\r\n    int a , b;\r\n    cin >> a >> b;\r\n    cout << suma(a , b);\r\n    \r\n    return 0;\r\n}', 'suma', NULL, NULL),
(12, 1, NULL, NULL, 129854, '2024-05-30 16:38:24', '#include <iostream>\r\nusing namespace std;\r\n\r\nint main()\r\n{\r\n    int * x;\r\n    x = new int[3];\r\n    cin >> x[0] >> x[1];\r\n    x[2] = x[0] + x[1];\r\n    cout << x[2];\r\n    return 0;\r\n}', 'suma', NULL, NULL),
(13, 1, NULL, NULL, 129854, '2024-05-30 16:40:01', '#include <iostream>\r\nusing namespace std;\r\nint a,b;\r\nint main() \r\n{\r\n    cin>>a>>b; \r\n    cout<<a-b;\r\n    return 0; \r\n}', 'diferenta', NULL, NULL),
(14, 1, NULL, NULL, 129854, '2024-05-30 16:42:09', '#include <iostream>\r\nusing namespace std;\r\nint a , b , d;\r\nint main() \r\n{\r\n    cin >> a >> b;\r\n    d = a - b;\r\n    cout << d;\r\n    return 0; \r\n}', 'diferenta', NULL, NULL),
(15, 1, NULL, NULL, 129854, '2024-05-30 16:46:48', '#include <iostream>\r\nusing namespace std;\r\nint a , b , d;\r\nint main() \r\n{\r\n    cin >> a >> b;\r\n    d = a - b;\r\n    cout << d;\r\n    return 0; \r\n}', 'diferenta', NULL, NULL),
(16, 1, NULL, NULL, 129854, '2024-05-30 16:53:01', '#include <iostream>\r\nusing namespace std;\r\nint a , b , d;\r\nint main() \r\n{\r\n    cin >> a >> b;//citim numerele\r\n    d = a - b;//facem diferenta lor\r\n    cout << d;//afisam diferenta\r\n    return 0; \r\n}', 'diferenta', NULL, NULL),
(17, 1, NULL, NULL, 765438, '2024-06-05 08:55:27', 'buna ziua!', 'suma', NULL, NULL),
(18, 15, 'tema', 'tema', NULL, '2024-06-05 08:57:04', NULL, NULL, NULL, NULL),
(19, 16, 'tema', 'tema1', NULL, '2024-06-05 09:04:56', NULL, NULL, NULL, NULL),
(20, 1, NULL, NULL, 129854, '2024-06-05 09:06:39', 'raspuns', 'tema1', NULL, NULL),
(21, 1, NULL, NULL, 765438, '2024-06-14 10:30:39', 'aaa', 'tema1', NULL, NULL),
(22, 1, NULL, NULL, 613166, '2024-06-18 17:01:20', 'suma', 'suma', NULL, NULL);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `tutore`
--

DROP TABLE IF EXISTS `tutore`;
CREATE TABLE IF NOT EXISTS `tutore` (
  `ID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `tutore`
--

INSERT INTO `tutore` (`ID`, `Username`, `Password`) VALUES
(654321, 'AndreiIonescu55', 'zxcv45hbnm'),
(951753, 'GabrielaPopa44', 'lkjh32qazx'),
(357159, 'IonMihai66', 'nbvc78asdf'),
(258369, 'AlexDragomir77', 'mnbv69plk'),
(147258, 'LauraBalan88', 'qwerty12ui'),
(369852, 'AndreeaPopescu10', 'pgtfb56zxc'),
(784512, 'CatalinDumitru09', 'zxcv45asd'),
(365214, 'MariaIlinca13', 'asdf98qwer'),
(613165, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `tutori_cursuri`
--

DROP TABLE IF EXISTS `tutori_cursuri`;
CREATE TABLE IF NOT EXISTS `tutori_cursuri` (
  `tutore_id` int(11) NOT NULL,
  `curs_id` int(11) NOT NULL,
  PRIMARY KEY (`tutore_id`,`curs_id`),
  KEY `curs_id` (`curs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `tutori_cursuri`
--

INSERT INTO `tutori_cursuri` (`tutore_id`, `curs_id`) VALUES
(357159, 3),
(613165, 4),
(654321, 1),
(951753, 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
