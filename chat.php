<?php
session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['role'])) {
    header("Location: login.html");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

$username = $_SESSION['username'];
$role = $_SESSION['role'];

$users = array();

if ($role == 'student') {
    $stmt = $conn->prepare("SELECT tutore.id, tutore.username 
                            FROM tutore
                            JOIN tutori_cursuri ON tutore.id = tutori_cursuri.tutore_id
                            JOIN studenti_cursuri ON tutori_cursuri.curs_id = studenti_cursuri.curs_id
                            JOIN studenti ON studenti_cursuri.student_id = studenti.id
                            WHERE studenti.username = ?");
    if ($stmt === false) {
        die("Eroare interogare SQL: " . $conn->error);
    }
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $users[] = array('id' => $row['id'], 'username' => $row['username'], 'role' => 'tutore');
    }
} else if ($role == 'tutore') {
    $stmt = $conn->prepare("SELECT studenti.id, studenti.username 
                            FROM studenti
                            JOIN studenti_cursuri ON studenti.id = studenti_cursuri.student_id
                            JOIN tutori_cursuri ON studenti_cursuri.curs_id = tutori_cursuri.curs_id
                            JOIN tutore ON tutori_cursuri.tutore_id = tutore.id
                            WHERE tutore.username = ?");
    if ($stmt === false) {
        die("Eroare interogare SQL: " . $conn->error);
    }
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $users[] = array('id' => $row['id'], 'username' => $row['username'], 'role' => 'student');
    }
}

$stmt->close();
$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<style>
        header{
        background-color: rgb(11, 0, 106);
        text-align: center;
        width: 195vh;
        height: 100px;
        font-size: 80px;
        color: aliceblue;
        border-radius: 10px;
        }
    
        #chat-box {
            width: 75%;
            height: 300px;
            border: 1px solid #ccc;
            overflow-y: scroll;
            margin: 20px 0;
            margin-bottom: 0px;
            padding: 10px;
        }
        #message-input {
            width: 75%;
            padding: 10px;
        }
        .user-list {
            list-style-type: none;
        }
        .user-list li {
            cursor: pointer;
            padding: 5px;
            border-bottom: 1px solid navy;
            font-size: 17px;
        }
        .user-list li:hover {
            background-color: navy;
            color: white;
        }
        .message {
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 5px;
            max-width: 80%;
        }
        .message.sent {
            text-align: right;
            background-color: navy;
            color:white;
            margin-left: auto;
        }
        .message.received {
            text-align: left;
            background-color: #f1f1f1;
        }
        .message .username {
            font-weight: bold;
            font-style: italic;
        }
        #back-link {
            display: block;
            margin-bottom: 20px;
        }
        .iesire{
            margin-left: 1400px;
            font-size: 20px;
        }
        button:hover{
            background-color: navy;
            color: white;
            border-color: navy;
        }
        button{
            background-color: white;
            color: navy;
            font-size: 17px;
            border: 2px solid navy;
            font-family: 'Times New Roman', Times, serif;
        }

        
        
    </style>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>UPT TUTORING</title>
  <header>
    Welcome UPT Tutoring!

</header>
</head>
<body>
<a href="javascript:void(0);" onclick="goBack()" class="iesire">Iesire</a>
<h3 style="font-size:25px;margin-top: 0px;">Trimite mesaj:</h3>


<ul class="user-list">
    <?php foreach ($users as $user): ?>
        <li onclick="selectUser(<?php echo $user['id']; ?>, '<?php echo htmlspecialchars($user['username']); ?>')"><?php echo htmlspecialchars($user['username']); ?></li>
    <?php endforeach; ?>
</ul>

<div id="chat-container" style="display: none;">
    <span id="chat-with"></span>
    <div id="chat-box"></div>
    <input type="text" id="message-input" placeholder="Mesaj...">
    <br><br>
    <button onclick="sendMessage()">Trimite</button>
</div>

<script>
    let selectedUserId = null;
    let currentUser = '<?php echo $username; ?>';

    function goBack() {
        window.history.back();
    }

    function selectUser(userId, username) {
        selectedUserId = userId;
        document.getElementById('chat-with').innerText = username;
        document.getElementById('chat-container').style.display = 'block';
        fetchMessages();
    }

    function fetchMessages() {
        if (selectedUserId === null) return;

        const xhr = new XMLHttpRequest();
        xhr.open('GET', 'fetch_messages.php?receiver_id=' + selectedUserId, true);
        xhr.onload = function () {
            if (this.status === 200) {
                const messages = JSON.parse(this.responseText);
                let chatBox = document.getElementById('chat-box');
                chatBox.innerHTML = '';
                messages.forEach(function (message) {
                    let messageElement = document.createElement('div');
                    messageElement.classList.add('message');
                    if (message.sender_username === currentUser) {
                        messageElement.classList.add('sent');
                    } else {
                        messageElement.classList.add('received');
                    }
                    messageElement.innerHTML = '<div class="username">' + message.sender_username + '</div><div class="text">' + message.message + '</div>';
                    chatBox.appendChild(messageElement);
                });
                chatBox.scrollTop = chatBox.scrollHeight;
            }
        };
        xhr.send();
    }

    function sendMessage() {
        if (selectedUserId === null) return;

        const message = document.getElementById('message-input').value;
        if (message === '') return;

        const xhr = new XMLHttpRequest();
        xhr.open('POST', 'send_message.php', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            if (this.status === 200) {
                document.getElementById('message-input').value = '';
                fetchMessages();
            }
        };
        xhr.send('receiver_id=' + selectedUserId + '&message=' + encodeURIComponent(message));
    }

    setInterval(fetchMessages, 3000); 
</script>



</body>
</html>
