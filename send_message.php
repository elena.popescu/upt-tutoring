<?php
session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['role'])) {
    header("Location: login.html");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

$username = $_SESSION['username'];
$role = $_SESSION['role'];

$user_id = null;

if ($role == 'student') {
    $stmt = $conn->prepare("SELECT id FROM studenti WHERE username = ?");
    $stmt->bind_param("s", $username);
} else if ($role == 'tutore') {
    $stmt = $conn->prepare("SELECT id FROM tutore WHERE username = ?");
    $stmt->bind_param("s", $username);
}

$stmt->execute();
$result = $stmt->get_result();
if ($row = $result->fetch_assoc()) {
    $user_id = $row['id'];
}

$receiver_id = $_POST['receiver_id'];
$message = $_POST['message'];

$stmt = $conn->prepare("INSERT INTO messages (sender_id, receiver_id, message) VALUES (?, ?, ?)");
$stmt->bind_param("iis", $user_id, $receiver_id, $message);
$stmt->execute();

$stmt->close();
$conn->close();
?>
