<?php
session_start();

if (!isset($_SESSION['username']) || $_SESSION['role'] != 'student') {
    header("Location: login.html");
    exit();
}

if (!isset($_GET['curs_id'])) {
    $_SESSION['notification'] = ['message' => 'Cursul nu a fost specificat.','type' => 'error'];
    header("Location: index.php");
    exit();
}

$curs_id = intval($_GET['curs_id']);

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

$stmt = $conn->prepare("SELECT titlu FROM cursuri WHERE id = ?");
if (!$stmt) {
    die("Eroare la pregătirea interogării: " . $conn->error);
}
$stmt->bind_param("i", $curs_id);
$stmt->execute();
$curs_result = $stmt->get_result();
if ($curs_result->num_rows > 0) {
    $curs = $curs_result->fetch_assoc();
    $titlu_curs = htmlspecialchars($curs['titlu']);
} else {
    $_SESSION['notification'] = ['message' => 'Cursul nu a fost gasit.','type' => 'error'];
    header("Location: index.php");
    exit();
}
$stmt->close();

$stmt = $conn->prepare("SELECT lectii.id, lectii.titlu FROM lectii 
                        JOIN cursuri_lectii ON lectii.id = cursuri_lectii.lectie_id 
                        WHERE cursuri_lectii.curs_id = ?");
if (!$stmt) {
    $_SESSION['notification'] = ['message' => 'Eroare la pregatirea interogarii pentru lectii:','type' => 'error'];
    header("Location: index.php");
}
$stmt->bind_param("i", $curs_id);
$stmt->execute();
$lectii_result = $stmt->get_result();
?>

<!DOCTYPE html>
<style>
    header{
        background-color: rgb(11, 0, 106);
        text-align: center;
        width: 195vh;
        height: 100px;
        font-size: 80px;
        color: aliceblue;
        border-radius: 10px;
    
    }
    .login-page{
        background-color: rgb(212, 235, 255);
    margin-top:15px;
    margin-left: 150px;
    margin-right: 150px;
    text-align: center;
    height: 550px;
    font-size: 60px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 10px;
    }
    .detalii{
        font-family: 'Times New Roman', Times, serif;
        font-style:italic;
        font-size: 40 px;
        color:rgb(11, 0, 106);
        border-bottom-style: solid;
        
    }
   
   .butoane_titlu{
    border-width: 0px;
    font-family: 'Times New Roman', Times, serif;
    font-size: 30px;
    font-style: italic;
    color: navy;
   
   }
    
    
</style>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UPT TUTORING</title>
    <header>
        Welcome UPT Tutoring!
    </header>
</head>
<body>
    <h2 class="detalii"> Detalii Curs: <?php echo $titlu_curs; ?>
    </h2>
    <div class="section lectii">
        <h2 style="font-style:italic">Lecții</h2>
        <ul>
        <?php
        if ($lectii_result->num_rows > 0) {
            echo "<ul>";
            while ($lectie = $lectii_result->fetch_assoc()) {
                echo "<li>";
                echo "<a href='lectie.php?lectie_id={$lectie['id']}' style='font-size:30px; color:navy;font-style:italic;'>" . htmlspecialchars($lectie['titlu']) . "</a>";
                $lectie_id = $lectie['id'];
                $stmt_teme = $conn->prepare("SELECT titlu, continut FROM teme WHERE lectie_id = ?");
                $stmt_teme->bind_param("i", $lectie_id);
                $stmt_teme->execute();
                $teme_result = $stmt_teme->get_result();
                if ($teme_result->num_rows > 0) {
                    echo "<ul style='font-size: 25px;font-style:italic;'>TEMA:";
                    while ($tema = $teme_result->fetch_assoc()) {
                        if ($tema['continut'] != NULL) {
                            echo "<li style='font-size: 20px;font-style:italic;font-weight:normal'><strong>" . htmlspecialchars($tema['titlu']) . ":</strong> " . htmlspecialchars($tema['continut']) . "</li>";
                            echo "<a href='upload_form.php?curs_id={$curs_id}' style='font-size: 17px;font-style:italic;font-weight:normal'>Incarca tema</a><br><br>";
                        }
                    }
                    echo "</ul>";
                } 
                echo "</li>";
            }
            echo "</ul>";
        } else {
            $_SESSION['notification'] = ['message' => 'Nu exista lectii disponibile pentru acest curs.','type' => 'error'];
            header("Location: index.php");
        }
        ?>
        </ul>
    </div>
    <br>
    <a href="student.php" style="color:blue">Inapoi la pagina principala</a>
</body>
</html>

<?php
$stmt->close();
$conn->close();
?>
