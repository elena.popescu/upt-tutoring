<?php
session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['profesor_logged_in'])) {
    $_SESSION['notification'] = ['message' => 'Trebuie să vă autentificați ca profesor.', 'type' => 'error'];
    header("Location: index.php");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

function displayNotification() {
    if (isset($_SESSION['notification'])) {
        echo '<div style="padding: 10px; background-color: #f44336; color: white; text-align: center;">';
        echo $_SESSION['notification']['message'];
        echo '</div>';
        unset($_SESSION['notification']);
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
    $inputUsername = $_POST['username'];
    $inputPassword = $_POST['password'];

    $stmt = $conn->prepare("SELECT * FROM profesori WHERE username = ? AND password = ?");
    $stmt->bind_param("ss", $inputUsername, $inputPassword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $_SESSION['username'] = $inputUsername;
        $_SESSION['profesor_logged_in'] = true;
    } else {
        $_SESSION['notification'] = ['message' => 'Eroare: Username sau parolă invalidă', 'type' => 'error'];
        header("Location: index.php");
        exit();
    }

    $stmt->close();
}

$searchTerm = '';
$selectCurs = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['search'])) {
        $searchTerm = $conn->real_escape_string($searchTerm);
    }
    if (isset($_POST['select_curs'])) {
        $selectCurs = $_POST['select_curs'];
    }
}

$queryCursuri = "
    SELECT c.id AS curs_id, c.titlu AS curs_titlu, 
           s.username AS student_username, 
           t.nume_tema, 
           t.raspuns, 
           t.feedback, 
           l.titlu AS lectie_titlu, 
           l.continut AS lectie_continut, 
           tu.username AS tutore_username, 
           t.continut AS cerinta
    FROM cursuri c
    LEFT JOIN cursuri_lectii cl ON c.id = cl.curs_id
    LEFT JOIN lectii l ON cl.lectie_id = l.id
    LEFT JOIN teme t ON l.id = t.lectie_id
    LEFT JOIN studenti s ON t.student_id = s.id
    LEFT JOIN tutori_cursuri tc ON c.id = tc.curs_id
    LEFT JOIN tutore tu ON tc.tutore_id = tu.id";

if ($selectCurs) {
    $queryCursuri .= " WHERE c.id = $selectCurs";
} elseif ($searchTerm) {
    $queryCursuri .= " WHERE c.titlu LIKE '%$searchTerm%'";
}

$queryCursuri .= " ORDER BY c.id";

$resultCursuri = $conn->query($queryCursuri);

if (!$resultCursuri) {
    die('Eroare la executarea interogării: ' . $conn->error);
}

$stmt_profesori = $conn->prepare("SELECT username FROM profesori");
if (!$stmt_profesori) {
    die('Eroare la pregătirea interogării pentru activitatea altor profesori: ' . $conn->error);
}
$stmt_profesori->execute();
$result_profesori = $stmt_profesori->get_result();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>UPT TUTORING</title>
  <style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f2f2f2;
        margin: 20px;
    }
    .container {
        max-width: 950px;
        margin: auto;
        background-color: white;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0,0,0,0.1);
    }
    .section {
        margin-bottom: 20px;
    }
    .section h2 {
        color: #333;
        border-bottom: 1px solid #ccc;
        padding-bottom: 5px;
    }
    table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 10px;
    }
    table, th, td {
        border: 1px solid #ccc;
    }
    th, td {
        padding: 10px;
        text-align: left;
    }
    th {
        background-color: #f2f2f2;
    }
    .search-bar {
        margin-bottom: 20px;
    }
    .search-bar select, .search-bar input[type="text"], .search-bar button {
        margin-right: 10px;
        margin-bottom: 10px;
        padding: 5px;
        font-size: 14px;
    }
    header {
        background-color: rgb(11, 0, 106);
        text-align: center;
        width: 195vh;
        height: 100px;
        font-size: 80px;
        color: aliceblue;
        border-radius: 10px;
    }
  </style>
</head>
<body>
    <header>
        Welcome UPT Tutoring!
    </header>
    <div class="container">
        <h1 style="text-align: center;">Activitate Profesor</h1>

        <?php displayNotification(); ?>

        <div class="search-bar">
            <form method="POST" action="">
                <select name="select_curs">
                    <option value="">Selectați un curs</option>
                    <?php
                    $queryCursuriDropdown = "SELECT id, titlu FROM cursuri ORDER BY titlu";
                    $resultCursuriDropdown = $conn->query($queryCursuriDropdown);
                    if ($resultCursuriDropdown->num_rows > 0) {
                        while ($rowDropdown = $resultCursuriDropdown->fetch_assoc()) {
                            $selected = ($selectCurs == $rowDropdown['id']) ? 'selected' : '';
                            echo '<option value="' . htmlspecialchars($rowDropdown['id']) . '" ' . $selected . '>';
                            echo htmlspecialchars($rowDropdown['titlu']);
                            echo '</option>';
                        }
                    }
                    ?>
                </select>
                <button type="submit" name="search">Căutare</button>
            </form>
        </div>

        <div class="section">
            <h2>Activitatea Studenților</h2>
            <?php if ($resultCursuri->num_rows > 0): ?>
                <?php
                $currentCursId = null;
                while ($row = $resultCursuri->fetch_assoc()):
                    if ($row['curs_id'] !== $currentCursId):
                        if ($currentCursId !== null):
                            echo '</table></div>';
                        endif;
                        $currentCursId = $row['curs_id'];
                ?>
                <div class="section">
                    <h3><?php echo htmlspecialchars($row['curs_titlu']); ?></h3>
                    <table>
                        <tr>
                            <th>Student</th>
                            <th>Temă</th>
                            <th>Răspuns</th>
                            <th>Feedback</th>
                            <th>Lecție</th>
                            <th>Conținut Lecție</th>
                            <th>Tutore Lecție</th>
                            <th>Cerință</th>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td><?php echo htmlspecialchars($row['student_username'] ?? '-'); ?></td>
                        <td><?php echo htmlspecialchars($row['nume_tema'] ?? '-'); ?></td>
                        <td><?php echo nl2br(htmlspecialchars($row['raspuns'] ?? '-')); ?></td>
                        <td><?php echo htmlspecialchars($row['feedback'] ?? '-'); ?></td>
                        <td><?php echo htmlspecialchars($row['lectie_titlu'] ?? '-'); ?></td>
                        <td><?php echo nl2br(htmlspecialchars($row['lectie_continut'] ?? '-')); ?></td>
                        <td><?php echo htmlspecialchars($row['tutore_username'] ?? '-'); ?></td>
                        <td><?php echo nl2br(htmlspecialchars($row['cerinta'] ?? '-')); ?></td>
                    </tr>
                <?php endwhile; ?>
                </table>
            </div>
            <?php else: ?>
                <p>Nu există teme încărcate pentru cursurile selectate.</p>
            <?php endif; ?>
        </div>

        <div class="section">
            <h2>Alți Profesori</h2>
            <?php if ($result_profesori->num_rows > 0): ?>
                <ul>
                    <?php while ($row = $result_profesori->fetch_assoc()): ?>
                        <li><?php echo htmlspecialchars($row['username']); ?></li>
                    <?php endwhile; ?>
                </ul>
            <?php else: ?>
                <p>Nu există alți profesori în sistem.</p>
            <?php endif; ?>
        </div>

        <div style="text-align: center; margin-top: 20px;">
            <a href="logout.php" style="padding: 10px 20px; background-color: #f44336; color: white; text-decoration: none; border-radius: 5px;">Logout</a>
        </div>
    </div>
</body>
</html>

<?php
$conn->close();
?>
