# UPT Tutoring - Monitorizarea și Digitalizarea Proiectelor "Tutore pentru Colegii Mai Tineri" și "Big Brother pentru Colegii din Anul I AC"

UPT Tutoring este o aplicație Web dedicată sprijinirii și digitalizării proiectelor universității care vizează mentoratul și tutoriatul între studenți.

## Link-uri Utile

- [Descărcare WampServer](https://sourceforge.net/projects/wampserver/)
- [Descărcare Visual Studio Code](https://code.visualstudio.com/download)

## Ghid de Configurare

### Pasul 1: Descărcare și Instalare WampServer

Pentru a utiliza UPT Tutoring, este necesar să descărcați și să instalați WampServer, care poate fi găsit la [acest link](https://sourceforge.net/projects/wampserver/).

### Pasul 2: Configurare PHP și MySQL

Asigurați-vă că versiunea de PHP este setată la 8.0.26 și MySQL la 5.7.40.

### Pasul 3: Descărcare și Configurare Visual Studio Code

Descărcați și instalați Visual Studio Code de la [acest link](https://code.visualstudio.com/download). Acesta va fi utilizat ca editor de cod pentru dezvoltarea aplicației UPT Tutoring.

### Pasul 4: Clonare Repository

```sh
git clone https://gitlab.upt.ro/elena.popescu/upt-tutoring

```
Salvați fișierele într-un folder cu numele `Licenta`
Navigați în directorul WAMP și plasați folderul cu fișierele clonate în folderul `www`.

### Pasul 5: Import Bază de Date

- Accesați phpMyAdmin prin intermediul WampServer.
- Creați o bază de date nouă.
- Importați fișierul `licenta.sql` din directorul clonat pentru a inițializa schema bazei de date necesară pentru funcționarea aplicației.

### Pasul 6: Configurare Aplicație

- Actualizați detaliile conexiunii la baza de date.

```php
<?php
$conn = new mysqli("localhost", "root", "", "Licenta");
?>
```

### Pasul 7: Lansare Aplicație

- Porniți serverul WampServer.
- Accesați aplicația în browser la adresa `http://localhost/Licenta/index.html`.

## Utilizare

- Pentru Studenți: Înregistrați-vă și puteți accesa cursurile la care sunteți înscriși.
- Pentru Tutori: Înregistrați-vă și oferiți asistență studenților prin încărcați resurse educaționale.
- Pentru Profesori: Monitorizați activitățile de tutoriat și revizuiți materialele încărcate de tutori.

## Observație
- Toți utilizatorii sunt intruduși deja în baza de date deoarece aceștia se conectează cu contul de student.

## Autori

- [Popescu Elena-Cosmina](https://gitlab.upt.ro/elena.popescu)
