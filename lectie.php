<?php
session_start();

if (!isset($_SESSION['username']) || $_SESSION['role'] != 'student') {
    header("Location: login.html");
    exit();
}

if (!isset($_GET['lectie_id'])) {
    echo "Lecția nu a fost specificată.";
    exit();
}

$lectie_id = intval($_GET['lectie_id']);

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

$stmt = $conn->prepare("SELECT titlu, continut FROM lectii WHERE id = ?");
if (!$stmt) {
    die("Eroare la pregătirea interogării: " . $conn->error);
}
$stmt->bind_param("i", $lectie_id);
$stmt->execute();
$lectie_result = $stmt->get_result();
if ($lectie_result->num_rows > 0) {
    $lectie = $lectie_result->fetch_assoc();
    $titlu_lectie = htmlspecialchars($lectie['titlu']);
    $continut_lectie = htmlspecialchars($lectie['continut']);
} else {
    echo "Lecția nu a fost găsită.";
    exit();
}
$stmt->close();
?>

<!DOCTYPE html>
<style>
    header{
        background-color: rgb(11, 0, 106);
        text-align: center;
        width: 195vh;
        height: 100px;
        font-size: 80px;
        color: aliceblue;
        border-radius: 10px;
    
    }
    .login-page{
        background-color: rgb(212, 235, 255);
    margin-top:15px;
    margin-left: 150px;
    margin-right: 150px;
    text-align: center;
    height: 550px;
    font-size: 60px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 10px;
    }
    .section 
    {
            margin: 20px;
    }
    .continut {
            background-color: rgb(212, 235, 255);
            padding: 20px;
            border-radius: 10px;
    }
    </style>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>UPT TUTORING</title>
  <header>
    Welcome UPT Tutoring!
</header>
</head>
<body>
    <h1><?php echo $titlu_lectie; ?></h1>
    
    
    <div class="section continut">
        <h2>Conținut</h2>
        <p><?php echo nl2br($continut_lectie); ?></p>
    </div>
    
    <br>
    <a href="javascript:history.back()">Înapoi</a>
</body>
</html>

<?php
$conn->close();
?>
