<?php
session_start();

if (!isset($_SESSION['username']) || $_SESSION['role'] != 'tutore') {
    header("Location: login.html");
    exit();
}

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

$stmt = $conn->prepare("SELECT id FROM tutore WHERE username = ?");
$stmt->bind_param("s", $_SESSION['username']);
$stmt->execute();
$result = $stmt->get_result();

$tutore_id = null;
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $tutore_id = $row['id'];
}
$stmt->close();

if ($tutore_id) {
    $stmt = $conn->prepare("SELECT cursuri.id, cursuri.titlu FROM cursuri 
                            JOIN tutori_cursuri ON cursuri.id = tutori_cursuri.curs_id 
                            WHERE tutori_cursuri.tutore_id = ?");
    $stmt->bind_param("i", $tutore_id);
    $stmt->execute();
    $cursuri_result = $stmt->get_result();
    $curs = $cursuri_result->fetch_assoc(); 
    $curs_id = $curs['id'];
    $curs_titlu = htmlspecialchars($curs['titlu']);
}
?>

<!DOCTYPE html>
<style>
    header{
        background-color: rgb(11, 0, 106);
        text-align: center;
        width: 195vh;
        height: 100px;
        font-size: 80px;
        color: aliceblue;
        border-radius: 10px;
    }
    
    .start{
        font-family: 'Times New Roman', Times, serif;
        font-style:italic;
        font-size: 30 px;
        color:rgb(11, 0, 106);
        border-bottom-style: solid;
    }
    .chat{
    border-radius: 0px;
    background-color:white;
    color:rgb(11, 0, 106);
    margin-left: 1100px;
    border-width: 0px;
    font-family: 'Times New Roman', Times, serif;
    margin-right: 0px;
    


   }
    h3{
        font-style: italic;
        font-size: 30px;
        font-family: 'Times New Roman', Times, serif;
        color: rgb(11, 0, 106);
    }
</style>
<html lang="en">
<head>
    <title>UPT TUTORING</title>
    <header>
        Welcome UPT Tutoring!
    </header>
</head>
<body>
<h2 class="start">Bun venit, <?php echo htmlspecialchars($_SESSION['username']); ?>!
<a href="chat.php" class="chat">Chat</a>
</h2>

<h3>Cursuri si lectii</h3>
<?php
if ($curs) {
    echo "<h4>{$curs_titlu}</h4>";

    $stmt = $conn->prepare("SELECT lectii.id, lectii.titlu FROM lectii 
                            JOIN cursuri_lectii ON lectii.id = cursuri_lectii.lectie_id 
                            WHERE cursuri_lectii.curs_id = ?");
    $stmt->bind_param("i", $curs_id);
    $stmt->execute();
    $lectii_result = $stmt->get_result();

    if($lectii_result->num_rows > 0) {
        echo "<ul>";
        while ($lectie = $lectii_result->fetch_assoc()) {
            echo "<li>" . htmlspecialchars($lectie['titlu']) . " 
                  <a href='tutore.php?delete_lectie_id=" . $lectie['id'] . "'>Șterge lectia</a></li>";
        }
        echo "</ul>";
    } else {
        echo "<p>No lessons available for this course.</p>";
    }
} else {
    echo "<p>No courses assigned.</p>";
}
?>

<h3>Adauga lectie</h3>
<form action="tutore.php" method="POST">
   <label style="font-size:20px;">Titlu:</label><input type="text" name="titlu" required style="margin-left: 30px;"><br>
   <label style="font-size:20px;">Continut:</label><textarea name="continut" required style="width: 50%; height: 400px "></textarea><br>
    <input type="submit" name="adauga_lectie" value="Adauga">
</form>

<?php

if (isset($_GET['delete_lectie_id'])) {
    $delete_lectie_id = $_GET['delete_lectie_id'];
    $stmt = $conn->prepare("DELETE FROM lectii WHERE id = ? AND EXISTS 
                            (SELECT * FROM cursuri_lectii WHERE lectii.id = cursuri_lectii.lectie_id 
                            AND cursuri_lectii.curs_id = ?)");
    $stmt->bind_param("ii", $delete_lectie_id, $curs_id);
    $stmt->execute();
    header("Location: tutore.php");
    exit();
}

if (isset($_POST['adauga_lectie'])) {
    $titlu = $_POST['titlu'];
    $continut = $_POST['continut'];
    $stmt = $conn->prepare("INSERT INTO lectii (titlu, continut) VALUES (?, ?)");
    $stmt->bind_param("ss", $titlu, $continut);
    $stmt->execute();
    $lectie_id = $stmt->insert_id;
    $stmt->close();

    $stmt = $conn->prepare("INSERT INTO cursuri_lectii (curs_id, lectie_id) VALUES (?, ?)");
    $stmt->bind_param("ii", $curs_id, $lectie_id);
    $stmt->execute();
    $stmt->close();
    header("Location: tutore.php");
    exit();
}
?>
<h3>Adaugă temă</h3>
<form action="adauga_tema.php" method="POST">
    <select name="lectie_id" required>
        <option value="">Selectează lecția</option>
        <?php
        $stmt = $conn->prepare("SELECT lectii.id, lectii.titlu FROM lectii 
                                JOIN cursuri_lectii ON lectii.id = cursuri_lectii.lectie_id 
                                JOIN tutori_cursuri ON cursuri_lectii.curs_id = tutori_cursuri.curs_id 
                                WHERE tutori_cursuri.tutore_id = ?");
        $stmt->bind_param("i", $tutore_id);
        $stmt->execute();
        $lectii_result = $stmt->get_result();
        
        while ($lectie = $lectii_result->fetch_assoc()) {
            echo "<option value='" . $lectie['id'] . "'>" . htmlspecialchars($lectie['titlu']) . "</option>";
        }
        ?>
    </select><br>
    Titlu tema:<input type="text" name="titlu_tema"required>
    Conținut temă: <textarea name="continut_tema" required></textarea><br>
    <input type="submit" value="Adaugă temă">
</form>
<a href='view_tema.php?curs_id=<?php echo $curs_id; ?>'>Vezi tema</a>";


<br><br>
</body>
</html>
