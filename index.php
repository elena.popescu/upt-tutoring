<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pagina Principală</title>
    <style>
        .notification {
            padding: 10px;
            margin: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: 1000px;
            
            text-align: center;
        }
        .success {
            background-color: #dff0d8;
            color: #3c763d;
        }
        .error {
            background-color: #f2dede;
            color: #a94442;
        }
    </style>
</head>
<body>
    <?php
    if (isset($_SESSION['notification'])) {
        $notification = $_SESSION['notification'];
        echo "<div class='notification {$notification['type']}'>{$notification['message']}</div>";
        unset($_SESSION['notification']);
    }
    ?>
    <a href="javascript:void(0);" onclick="goBack()" >Inapoi</a>
    <script>
     function goBack() {
        window.history.back();
    }
</script>
</body>
</html>
