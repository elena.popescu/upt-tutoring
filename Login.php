<?php
session_start();

$conn = new mysqli("localhost", "root", "", "Licenta");

if ($conn->connect_error) {
    die("Conexiune eșuată: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputUsername = $_POST['username'];
    $inputPassword = $_POST['password'];

    $stmt = $conn->prepare("SELECT * FROM studenti WHERE Username = ? AND Password = ?");
    $stmt->bind_param("ss", $inputUsername, $inputPassword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $_SESSION['username'] = $inputUsername;
        $_SESSION['role'] = 'student';
        header("Location: student.php");
        exit();
    }

    $stmt = $conn->prepare("SELECT * FROM tutore WHERE Username = ? AND Password = ?");
    $stmt->bind_param("ss", $inputUsername, $inputPassword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $_SESSION['username'] = $inputUsername;
        $_SESSION['role'] = 'tutore';
        header("Location: tutore.php");
        exit();
    }

    $stmt = $conn->prepare("SELECT * FROM profesori WHERE Username = ? AND Password = ?");
    $stmt->bind_param("ss", $inputUsername, $inputPassword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $_SESSION['username'] = $inputUsername;
        $_SESSION['profesor_logged_in'] = true; 
        header("Location: profesor.php");
        exit();
    }

    $_SESSION['notification'] = ['message' => 'Eroare: Username sau parolă invalidă', 'type' => 'error'];
    header("Location: index.php");

    $stmt->close();
}

$conn->close();
?>
